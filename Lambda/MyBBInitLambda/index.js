var response = require('cfn-response');
var AWS = require('aws-sdk');
var elasticbeanstalkMyBB = new AWS.ElasticBeanstalk();
 
exports.handler = function(event, context) {
      console.log('REQUEST RECEIVED:\n');  
      console.log('Event:\n', JSON.stringify(event));
      console.log('Context:\n', JSON.stringify(context));

      if (event.RequestType != 'Create') {
            response.send(event, context, response.SUCCESS);
            return;
      }

      var paramsset = {
            EnvironmentName: event.ResourceProperties.MyBBBSEnvironment,
            OptionSettings: [{
                  "Namespace": "aws:elasticbeanstalk:application:environment", 
                  "OptionName": "AWS_MYBB_URL", 
                  "Value": "http://"+ event.ResourceProperties.MyBBHost
            },
            {
                  "Namespace": "aws:elasticbeanstalk:application:environment", 
                  "OptionName": "AWS_MYBB_COOCKIE_DOMAIN", 
                  "Value": "." + event.ResourceProperties.MyBBHost
            }
            ]
      };
      console.log('MyBB BS params:\n', JSON.stringify(paramsset));

      elasticbeanstalkMyBB.updateEnvironment(paramsset, function (err, data) {
            if (err) { 
                  console.error("Unable to update Beanstalk Environment. Error JSON:", JSON.stringify(err, null, 2));                              
                  response.send(event, context, response.FAILED);                  
                  } 
            else {
                  console.log("Beanstalk Environment succesfully updated:", JSON.stringify(data, null, 2));                             
                  response.send(event, context, response.SUCCESS);
                  }                      
      });
};