#!/bin/sh
# Script to delete S3 storage for beanstalk
aws s3api delete-bucket-policy --bucket elasticbeanstalk-us-east-1-323483035543
aws s3 rb s3://elasticbeanstalk-us-east-1-323483035543 --force