{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "MyBB Environment",
	"Parameters": {	
    "KeyName": {
      "Description": "Key Pair name",
      "Type": "AWS::EC2::KeyPair::KeyName",
      "Default": "mykey"
    },
    "EnvironmentType": {
      "Description": "Type of Environment",
      "Type": "String",
          "AllowedValues" : ["Prod", "Test"],
          "Default": "Test",
          "ConstraintDescription" : "must specify Prod or Test."
    },
    "AppPort" : {
      "Description": "The number of Application's TCP port to access for clients",
      "Type": "Number",
          "MinValue": "1",
          "MaxValue": "65535",
          "Default": "80"
    },
    "MinSize":{
      "Type":"Number",
      "Default":"1",
      "Description":"Minimum Number of instances to launch in Beanstalk Environment."
    },
    "MaxSize":{
      "Type":"Number",
      "Default":"1",
      "Description":"Maximum Number of instances to launch in Beanstalk Environment."
    },
    "VPCStack": {
      "Description": "Name of VPC stack",
      "Type": "String",
      "Default": "Test-VPC"
    },
    "DBStack": {
      "Description": "Name of Data DB stack",
      "Type": "String",
      "Default": "Test-DataDB"
    },
    "IAMStack": {
      "Description": "Name of IAM stack",
      "Type": "String",
      "Default": "Test-IAM"
    },  
 		"MyBBFilesS3LocationBucket": {
			"Description": "Location of MyBB files (S3 Bucket)",
			"Type": "String",
			"Default": "AndreyAsoskovtest"
		},     		
 		"MyBBFilesS3LocationCode": {
			"Description": "Location of MyBB Code (S3 Key)",
			"Type": "String",
			"Default": "mybb.1.8.10.cloud2.zip"      
		},
		"DBName" : {
			"Description" : "The name of Data DB that should be used for MyBB",
      "Type": "String",
      "MinLength" : "1",
    	"MaxLength" : "64",
			"AllowedPattern" : "[a-zA-Z0-9]*",
      "ConstraintDescription": "Must contain only AlphaNumeric Values (min8-max24)",
      "Default": "mybbdb"
    },    	
		"DBUser" : {
			"Description" : "The user that should be used in MyBB Data DB to access it",
      "Type": "String",
      "MinLength" : "1",
    	"MaxLength" : "16",
			"AllowedPattern" : "[a-zA-Z0-9]*",
      "ConstraintDescription": "Must contain only AlphaNumeric Values (min8-max24)",
      "Default": "mybbuser"
    },		
		"DBPassword" : {
			"Description" : "The password that should be used to access Data DB",
      "Type": "String",
      "MinLength" : "8",
    	"MaxLength" : "41",
      "NoEcho": "true",
			"AllowedPattern" : "[a-zA-Z0-9]*",
      "ConstraintDescription": "Must contain only AlphaNumeric Values (min8-max24)",
      "Default": "mybbpassword"
    },			
    "Email" : {
      "Description" : "Email for notifications",
      "Type": "String",
      "MinLength" : "5",
      "MaxLength" : "50",
      "Default": "aws.andrey.asoskov@gmail.com"
    } 
	},
	"Mappings": {
    "EC2RegionMap": {
      "us-east-1": {"AmazonLinuxHVM": "ami-9be6f38c","AmazonLinuxPV": "ami-c7e7f2d0"},
      "us-east-2": {"AmazonLinuxHVM": "ami-38cd975d"},  
      "us-west-1": {"AmazonLinuxHVM": "ami-b73d6cd7","AmazonLinuxPV": "ami-f33f6e93"},
      "us-west-2": {"AmazonLinuxHVM": "ami-1e299d7e","AmazonLinuxPV": "ami-a3299dc3"}
    }  
	},
	"Resources": {
    "SecureLogGroup": {
      "Type": "AWS::Logs::LogGroup",
      "Properties": {
        "LogGroupName": "/var/log/secure",
        "RetentionInDays": "1"
      }
    },

    "MySNSTopic" : {
      "Type" : "AWS::SNS::Topic",
      "Properties" : {
        "Subscription" : [ {
          "Endpoint" : {"Ref": "Email"},
          "Protocol" : "email"
        } ]
      }
    },
    "MyBBLatencyAlarm": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "EvaluationPeriods": "1",
        "Statistic": "Average",
        "Threshold": "100",
        "AlarmDescription": "Alarm when Latency exceeds 100s.",
        "Period": "300",
        "EvaluationPeriods": "3",
        "Unit": "Seconds",
        "AlarmActions": [ {"Ref": "MySNSTopic"} ],
        "Namespace": "AWS/ElasticBeanstalk ",
        "ComparisonOperator": "GreaterThanThreshold",
        "Dimensions": [
         {
            "Name": "Environment",
            "Value": { "Ref": "ElasticBeanstalkEnv" }
         }
        ],
        "MetricName": "ApplicationLatencyP99.9"
      }
    },
    "MyBBStatusAlarm": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "EvaluationPeriods": "1",
        "Statistic": "Average",
        "Threshold": "1",
        "AlarmDescription": "Alarm when Health Check is negative.",
        "Period": "300",
        "EvaluationPeriods": "3",
        "Unit": "None",
        "AlarmActions": [ {"Ref": "MySNSTopic"} ],
        "Namespace": "AWS/ElasticBeanstalk",
        "ComparisonOperator": "GreaterThanThreshold",
        "Dimensions": [
         {
            "Name": "Environment",
            "Value": { "Ref": "ElasticBeanstalkEnv" }
         }
        ],
        "MetricName": "EnvironmentHealth"
      }
    },


    "ElasticBeanstalkApp": {
      "Type": "AWS::ElasticBeanstalk::Application",
      "Properties": {
        "ApplicationName": "MyBB",
        "Description": "AWS Elastic Beanstalk MyBB Application"
      }
    },
   	"ElasticBeanstalkAppVer": {
     	"Type": "AWS::ElasticBeanstalk::ApplicationVersion",
      "Properties": {
        "ApplicationName": { "Ref": "ElasticBeanstalkApp" },
       	"Description": "MyBB ver.1.8.10",
        "SourceBundle": {
          			"S3Bucket": {"Ref": "MyBBFilesS3LocationBucket"},
          			"S3Key": {"Ref": "MyBBFilesS3LocationCode"}
        }
      }
    },
		"ElasticBeanstalkConfigurationTemplate": {
     	"Type": "AWS::ElasticBeanstalk::ConfigurationTemplate",
     	"Properties": {
       	"ApplicationName": { "Ref": "ElasticBeanstalkApp" },
        "SolutionStackName": "64bit Amazon Linux 2016.09 v2.3.1 running PHP 7.0",
       	"Description": "AWS ElasticBeanstalk MyBB Configuration Template V.1.0",
        "OptionSettings" : [
           {"Namespace" : "aws:autoscaling:launchconfiguration", "OptionName" : "EC2KeyName", "Value" : {"Ref": "KeyName"}},
           {"Namespace" : "aws:autoscaling:launchconfiguration", "OptionName" : "InstanceType", "Value" : "t2.micro"},           
           {"Namespace" : "aws:autoscaling:launchconfiguration", "OptionName" : "IamInstanceProfile", "Value": {"Fn::ImportValue" : {"Fn::Sub" : "${IAMStack}-MyBBBeanstalkIAMInstanceProfile"}}},                     
           {"Namespace" : "aws:autoscaling:launchconfiguration", "OptionName" : "SecurityGroups", "Value" : {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-BeanstalkInstanceSecurityGroup"}}},
           
           {"Namespace" : "aws:autoscaling:asg", "OptionName": "Availability Zones", "Value": "Any 2" }, 
           {"Namespace" : "aws:autoscaling:asg", "OptionName": "MinSize", "Value": {"Ref": "MinSize"} }, 
           {"Namespace" : "aws:autoscaling:asg", "OptionName": "MaxSize", "Value": {"Ref": "MaxSize"} },
           {"Namespace" : "aws:autoscaling:asg", "OptionName": "Cooldown", "Value": "60" },

           {"Namespace" : "aws:elb:loadbalancer", "OptionName" : "CrossZone", "Value" : "true"},
           {"Namespace" : "aws:elb:loadbalancer", "OptionName" : "SecurityGroups", "Value" : {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-LoadBalancerSecurityGroup"}}},
           {"Namespace" : "aws:elb:loadbalancer", "OptionName" : "ManagedSecurityGroup", "Value" : {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-LoadBalancerSecurityGroup"}}},

           {"Namespace" : "aws:elb:listener", "OptionName" : "InstancePort", "Value" : {"Ref": "AppPort"}},
          
           {"Namespace" : "aws:elb:policies", "OptionName" : "LoadBalancerPorts", "Value" : {"Ref": "AppPort"}},
           {"Namespace" : "aws:elb:policies", "OptionName" : "Stickiness Policy", "Value" : "true"},
           {"Namespace" : "aws:elb:policies", "OptionName" : "Stickiness Cookie Expiration", "Value" : "180"},
           {"Namespace" : "aws:elb:policies", "OptionName" : "ConnectionDrainingEnabled", "Value" : "true"},
           {"Namespace" : "aws:elb:policies", "OptionName" : "ConnectionDrainingTimeout", "Value" : "300"},



           {"Namespace" : "aws:elasticbeanstalk:cloudwatch:logs", "OptionName": "StreamLogs", "Value": "true"}, 
           {"Namespace" : "aws:elasticbeanstalk:cloudwatch:logs", "OptionName": "RetentionInDays", "Value": "1"}, 

           {"Namespace" : "aws:elasticbeanstalk:sns:topics", "OptionName": "Notification Protocol", "Value": "email"}, 
           {"Namespace" : "aws:elasticbeanstalk:sns:topics", "OptionName": "Notification Endpoint", "Value": {"Ref": "Email"}},           
           {"Namespace" : "aws:elasticbeanstalk:sns:topics", "OptionName": "Notification Topic Name", "Value": "MyBBBSNT"}, 


           {"Namespace" : "aws:elasticbeanstalk:healthreporting:system", "OptionName": "SystemType", "Value": "enhanced" },
           {"Namespace" : "aws:ec2:vpc", "OptionName" : "VPCId", "Value" : {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-VPCID"}}},
           {"Namespace" : "aws:ec2:vpc", "OptionName" : "AssociatePublicIpAddress", "Value" : "true"},
           {"Namespace" : "aws:ec2:vpc", "OptionName" : "Subnets", "Value" : { "Fn::Join" : [ "", 
              [
                {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PublicSubnetIDz1"}},
                ",",
                {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PublicSubnetIDz2"}}
              ]]} },
           {"Namespace" : "aws:ec2:vpc", "OptionName" : "ELBSubnets", "Value" : { "Fn::Join" : [ "", 
              [
                {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PublicSubnetIDz1"}},
                ",",
                {"Fn::ImportValue" : {"Fn::Sub" : "${VPCStack}-PublicSubnetIDz2"}}
              ]]} },
           {"Namespace" : "aws:elasticbeanstalk:environment", "OptionName": "EnvironmentType", "Value": "LoadBalanced"},
           {"Namespace" : "aws:elasticbeanstalk:environment", "OptionName": "ServiceRole", "Value": {"Fn::ImportValue" : {"Fn::Sub" : "${IAMStack}-BeanstalkServiceIAMRole"}}},          
           {"Namespace" : "aws:elasticbeanstalk:application", "OptionName": "Application Healthcheck URL", "Value": "HTTP:80/htaccess.txt"},  

           {"Namespace" : "aws:elasticbeanstalk:managedactions:platformupdate", "OptionName": "UpdateLevel", "Value": "minor"},  

          
           {"Namespace" : "aws:elasticbeanstalk:application:environment", "OptionName" : "AWS_MYBB_URL", "Value" : "http://localhost"},
           {"Namespace" : "aws:elasticbeanstalk:application:environment", "OptionName" : "AWS_MYBB_COOCKIE_DOMAIN", "Value" : "localhost"},
           {"Namespace" : "aws:elasticbeanstalk:application:environment", "OptionName" : "RDS_MYSQL_DB_NAME", "Value" : {"Ref": "DBName"}},
           {"Namespace" : "aws:elasticbeanstalk:application:environment", "OptionName" : "RDS_MYSQL_USERNAME", "Value" : {"Ref": "DBUser"}},
           {"Namespace" : "aws:elasticbeanstalk:application:environment", "OptionName" : "RDS_MYSQL_PASSWORD", "Value" : {"Ref": "DBPassword"}},
           {"Namespace" : "aws:elasticbeanstalk:application:environment", "OptionName" : "RDS_MYSQL_HOSTNAME", "Value" : {"Fn::ImportValue" : {"Fn::Sub" : "${DBStack}-DBEndPointAddress"}}}
        ]
     	}
   	},
		"ElasticBeanstalkEnv": {
      "Type": "AWS::ElasticBeanstalk::Environment",
      "DependsOn" : "SecureLogGroup", 
      "Properties": {
        "ApplicationName": { "Ref": "ElasticBeanstalkApp" },
        "Description": "AWS Elastic Beanstalk Environment running MyBB Application",
        "EnvironmentName": "MyBBAppEnv",
        "TemplateName": { "Ref": "ElasticBeanstalkConfigurationTemplate" },
        "SolutionStackName": "64bit Amazon Linux 2016.09 v2.3.1 running PHP 7.0",
        "VersionLabel": { "Ref": "ElasticBeanstalkAppVer" },
        "Tags": [{ "Key" : "EnvType", "Value" : {"Ref": "EnvironmentType"} }]
      }
    }
	},
	"Outputs": {
		"MyBBURL": {
      "Description": "MyBB URL",
			"Value": { "Fn::Join": ["", ["http://", { "Fn::GetAtt": ["ElasticBeanstalkEnv", "EndpointURL"] }]] },
      "Export" : { "Name" : {"Fn::Sub": "${AWS::StackName}-MyBBURL" }}      
		},
    "MyBBHost": {
      "Description": "MyBB Host name (Load Balancer)",
      "Value": { "Fn::Join": ["", [{ "Fn::GetAtt": ["ElasticBeanstalkEnv", "EndpointURL"] }]] },
      "Export" : { "Name" : {"Fn::Sub": "${AWS::StackName}-MyBBHost" }}      
    },
    "MyBBBeanstalkEnvironmentID" : {
      "Description" : "The ID of MyBB Beanstalk environment",
      "Value" :  { "Ref" : "ElasticBeanstalkEnv" },
      "Export" : { "Name" : {"Fn::Sub": "${AWS::StackName}-MyBBBeanstalkEnvironmentID" }}
    }
	}
}